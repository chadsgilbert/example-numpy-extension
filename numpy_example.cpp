#define PY_SSIZE_T_CLEAN
#include <Python.h>
#include <numpy/arrayobject.h>

#include <vector>

static PyObject* numpy_example_array(PyObject* self, PyObject* args)
{
	std::vector<double> vec = {3.4, 5.6, 33.4, 5.3};

	return Py_None;
}

static PyMethodDef numpy_example_methods[] = {
	{"array", (PyCFunction)numpy_example_array, METH_NOARGS, "Return a numpy array from C."},
	{NULL, NULL}
};

static struct PyModuleDef numpy_example_module = {
	PyModuleDef_HEAD_INIT,
	"numpy_example",
	"Example numpy extension module.",
	-1,
	numpy_example_methods
};

PyMODINIT_FUNC PyInit_numpy_example(void)
{
	PyObject* m = PyModule_Create(&numpy_example_module);
	import_array();
	return m;
}

