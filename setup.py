from distutils.core import setup, Extension

example = Extension('example',
        extra_compile_args=['-Wall','-Wextra','-g'],
        sources=['example.c'],
        define_macros=[("NPY_NO_DEPRECATED_API", "NPY_1_8_API_VERSION")]
        )

import numpy
example2 = Extension('numpy_example',
        language='C++',
        libraries=['stdc++'],
        extra_compile_args=['-std=c++17'],
        sources=['numpy_example.cpp'],
        include_dirs=[numpy.get_include()],
        define_macros=[("NPY_NO_DEPRECATED_API", "NPY_1_8_API_VERSION")]
        )

setup(
    name='example',
    version='0.1',
    description='Example python extension module.',
    author='Chad Gilbert',
    author_email='chad.s.gilert@gmail.com',
    ext_modules=[example, example2])

