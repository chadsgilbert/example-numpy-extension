#define PY_SSIZE_T_CLEAN
#include <Python.h>

static PyObject* example_hello(PyObject* self, PyObject* args, PyObject* kwargs)
{
	fprintf(stdout, "Hello World!\n");
	return Py_None;
}

static PyObject* example_error(PyObject* self, PyObject* args)
{
	PyErr_SetString(PyExc_RuntimeError, "Oh no! An Error occurred!");
	return NULL;
}

static PyObject* example_system(PyObject* self, PyObject* args)
{
	const char* command;
	int number;
	if (!PyArg_ParseTuple(args, "si", &command, &number)) {
		return NULL;
	}

	if (number < 0) {
		PyErr_SetString(PyExc_RuntimeError, "n must be non-negative");
		return NULL;
	}

	for (int i = 0; i < number; i++) {
		system(command);
	}
	return Py_None;
}

static PyObject* example_ohm(PyObject* self, PyObject* args, PyObject* kwargs)
{
	const char* keywords[] = {"", "current", "potential", "resistance", NULL};

	double tolerance, I, V, R;
	if (!PyArg_ParseTupleAndKeywords(args, kwargs, "dddd", keywords, &tolerance, &I, &V, &R))
	{
		return NULL;
	}
	
	if (fabs(V - I*R) < tolerance) {
		return Py_True;
	} else {
		return Py_False;
	}
}

static PyObject* example_tuple(PyObject* self, PyObject* args)
{
	return Py_BuildValue("(dl)s", 1.037, 4, "A very special tuple with another tuple in it.");
}

static PyObject* example_list(PyObject* self, PyObject* args)
{
	const int SIZE = 4;
	double list[] = {3.4, 5.6, 2.3, 6.7};
	PyObject* pylist = PyList_New(0);
	for (int i=0; i < SIZE; i++)
	{
		PyList_Append(pylist, Py_BuildValue("d",list[i]));
	}
	return pylist;
}

static PyObject* example_dict(PyObject* seld, PyObject* args)
{
	return Py_BuildValue("{s:l,s:O,s:d}",
			"my integer", 4,
			"my object", Py_True,
			"my float", 5.6);
}

static PyMethodDef example_methods[] = {
	{"hello", (PyCFunction)example_hello, METH_VARARGS | METH_KEYWORDS, "Say Hello"},
	{"error", (PyCFunction)example_error, METH_VARARGS, "Return an error."},
	{"system", (PyCFunction)example_system, METH_VARARGS, "Execute a system command"},
	{"ohm", (PyCFunction)example_ohm, METH_VARARGS | METH_KEYWORDS, "Test if some values match Ohm's law"},
	{"tuple", (PyCFunction)example_tuple, METH_NOARGS, "Return a very special tuple."},
	{"list", (PyCFunction)example_list, METH_NOARGS, "Return a C array as a list."},
	{"dict", (PyCFunction)example_dict, METH_NOARGS, "Return a dict."},
	{NULL, NULL}
};

static struct PyModuleDef example_module = {
	PyModuleDef_HEAD_INIT,
	"example",
	"Example python extension module.",
	-1,
	example_methods
};

PyMODINIT_FUNC PyInit_example(void)
{
	PyObject* m = PyModule_Create(&example_module);
	return m;
}

